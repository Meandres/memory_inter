mod parse_cfg;
mod solvers;
mod structs;
mod utils;

use crate::parse_cfg::*;
use crate::solvers::*;
use crate::structs::*;
use crate::utils::*;
use std::env;
use std::fs;

// For level 1. Returns the sum of the wcet + the number of memory accesses on the worst case
// memory access path multiplied by the worst delay of a memory access
fn estimated_memory_access_delay(nb_cores: u32, wcma_val: f64, wcet_val: f64, rrdelay: u32) -> f64 {
    return (((nb_cores - 1) * rrdelay) + 5) as f64 * wcma_val + wcet_val;
}

// Function that takes a path and returns a Program struct which contains the results of the wcma
// and the wcet analysis for this program.
fn analyse_cfg(path: &String) -> Option<Program> {
    let file = fs::read_to_string(path);
    let file_to_analyse;
    if file.is_ok() {
        file_to_analyse = file.ok().unwrap();
    } else {
        return None;
    }
    let mut cfg: Vec<BasicBlock> = parse_cfg_file(file_to_analyse);
    let max_level = finish_filling_fields(&mut cfg);
    let name = path.split(".").nth(0).unwrap().to_string();
    return Some(Program {
        name: name,
        wcma: worst_memory_access_path(&cfg, max_level),
        wcet: worst_execution_time_path(&cfg, max_level),
    });
}

// Estimate the wcet of a program considering the other program running on the other cores at the
// same time.
fn estimate_wcet(programs: &Vec<Program>, combination: &Vec<u32>, nb_cores: u32) -> Vec<f64> {
    let min_wcma = combination.iter().fold(0., |acc, el| {
        if acc == 0. || programs[*el as usize].wcma.obj_val < acc {
            programs[*el as usize].wcma.obj_val
        } else {
            acc
        } // this finds the smaller wcma of all the programs running.
    });
    combination.iter().fold(Vec::new(), |mut acc, el| {
        acc.push(estimated_memory_access_delay(
            nb_cores, // for each program compute its wcet
            min_wcma,
            programs[*el as usize].wcet.obj_val,
            10,
        ));
        acc
    })
}

fn main() {
    // By default computes the analysis of the two cfg given. Others can be given as arguments
    let mut args: Vec<String> = env::args().collect();
    let mut paths: Vec<String> = Vec::new();
    if args.len() == 1 {
        paths.push("DoorControl.cfg".to_string());
        paths.push("TDF.cfg".to_string());
    } else {
        args.remove(0);
        paths = args.clone();
    }
    let mut programs: Vec<Program> = Vec::new();
    for file in paths {
        let p: Option<Program> = analyse_cfg(&file);
        if p.is_some() {
            programs.push(p.unwrap());
        } else {
            println!("Error when opening file : {}", file);
        }
    }
    println!("\n\n\n\n\nStart of output :\n");
    println!("Analyse of file : {}", programs[0].name);
    // step 0 :
    println!(
        "The memory access adds an additional delay of {} cycles for this program",
        programs[0].wcma.obj_val * 5.
    );
    // Question 1
    if programs[0]
        .wcma
        .path
        .iter()
        .zip(programs[0].wcet.path.iter())
        .all(|(&x, &y)| x == y)
    {
        println!("The path for the wcma and the wcet are the same");
    } else {
        println!("The path for the wcma and the wcet are not the same");
        let mut different_bb: Vec<(u32, (u32, u32))> = Vec::new();
        programs[0]
            .wcma
            .path
            .iter()
            .zip(programs[0].wcet.path.iter())
            .enumerate()
            .for_each(|x| {
                let (nb, (a, e)) = x;
                if x.1 .0 != x.1 .1 {
                    different_bb.push((nb as u32, (*a as u32, *e as u32)));
                }
            });
        for dif_bb in different_bb {
            println!(
                "Bloc {} : WCMA {}, WCET {}",
                dif_bb.0, dif_bb.1 .0, dif_bb.1 .1
            );
        }
    }
    let min_cores: u32 = 2;
    let max_cores: u32 = 4;
    println!("\n\nMulticore analysis");
    for p in programs.iter().enumerate() {
        print!("{} : {}\t", p.0, p.1.name);
    }
    // Level 1:
    for nb_cores in min_cores..max_cores + 1 {
        println!(
            "Estimated worst case execution time with {} cores: {}",
            nb_cores,
            estimated_memory_access_delay(
                nb_cores,
                programs[0].wcma.obj_val,
                programs[0].wcet.obj_val,
                10
            )
        )
    }
    println!();
    // Level 2:
    let working_vec: Vec<u32> = (0..programs.len()).map(|x| x as u32).collect();
    for nb_cores in min_cores..max_cores + 1 {
        println!("On {} cores", nb_cores);
        let cross_product: Vec<Vec<u32>> =
            cross_product(&vec![working_vec.clone(); nb_cores as usize]);
        for comb in cross_product.iter() {
            let vec_of_wcet = estimate_wcet(&programs, comb, nb_cores as u32);
            print_vec_of_wcet(comb, vec_of_wcet);
        }
        println!();
    }
}
