use itertools::Itertools;
use std::collections::HashMap;

// Return true if two hash map contains the same key with the same value
fn hashmap_match(map1: &HashMap<&u32, usize>, map2: &HashMap<&u32, usize>) -> bool {
    for key in map1.keys() {
        match map2.get(key) {
            None => return false,
            Some(v) => {
                if map1.get(key).unwrap() != v {
                    return false;
                }
            }
        }
    }
    return true;
}

// Taken from https://rosettacode.org/wiki/Cartesian_product_of_two_or_more_lists#Rust
// since I couldn't figure out how to do it properly modified for my use
pub fn cross_product(lists: &Vec<Vec<u32>>) -> Vec<Vec<u32>> {
    let mut res: Vec<Vec<u32>> = vec![];

    let mut list_iter = lists.iter();
    if let Some(first_list) = list_iter.next() {
        for &i in first_list {
            res.push(vec![i]);
        }
    }
    for l in list_iter {
        let mut tmp = vec![];
        for r in res {
            for &el in l {
                let mut tmp_el = r.clone();
                tmp_el.push(el);
                tmp.push(tmp_el);
            }
        }
        res = tmp;
    }

    // Only keep one of each kind of permutations in the sets. We do this because we suppose that
    // all our cores are the same and thus it doesn't change anything to place a program on one
    // core or on another.
    let bind = res.clone();
    let numbers: Vec<(Vec<u32>, HashMap<&u32, usize>)> = bind
        .iter()
        .enumerate()
        .map(|el| (el.1.clone(), el.1.iter().counts()))
        .collect();
    let mut to_remove: Vec<(Vec<u32>, HashMap<&u32, usize>)> = Vec::new();
    for n in 0..numbers.len() {
        for n_c in n..numbers.len() {
            if hashmap_match(&(numbers[n].1), &(numbers[n_c].1)) && numbers[n] != numbers[n_c] {
                to_remove.push(numbers[n].clone());
            }
        }
    }
    res.retain(|x| to_remove.iter().all(|el| el.0 != *x));
    res
}
