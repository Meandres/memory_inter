use crate::structs::*;
use std::cmp::max;

// pretty print the cfg. not used in the program (commented out in the main).
pub fn print_cfg(cfg: &Vec<BasicBlock>) {
    for bb in cfg {
        println!("BB {} : ", bb.id,);
        print!("Successors : ");
        for i in 0..bb.succs.len() {
            print!("{:?} ", bb.succs[i]);
        }
        println!();
        print!("Predecessors : ");
        for i in 0..bb.preds.len() {
            print!("{:?} ", bb.preds[i]);
        }
        println!();
        println!("Level : {}", bb.level);
        println!("Number of memory access : {}", bb.mem_acc_nb);
        /*println!("\nContent :");
        for c in bb.content {
            println!("{}", c);
        }*/
        println!();
    }
}

// Used to print the results in level 2
pub fn print_vec_of_wcet(combination: &Vec<u32>, vec_wcet: Vec<f64>) {
    println!(
        "[{}]{} cycles",
        combination
            .iter()
            .fold("".to_string(), |acc, el| if acc == "" {
                format!("{}", el)
            } else {
                format!("{},{}", acc, el)
            }),
        vec_wcet
            .iter()
            .fold("".to_string(), |acc, el| format!("{}\t{}", acc, el))
    );
}

// Parses the cfg file given as argument
pub fn parse_cfg_file(content: String) -> Vec<BasicBlock> {
    let mut cfg: Vec<BasicBlock> = Vec::new();
    let mut current_index = 0;
    let lines = content.split('\n');
    lines.fold(Vec::new(), |mut acc, mut l| {
        l = l.trim_start();
        if l.starts_with("BB") {
            // new BasicBlock
            if current_index != 0 {
                cfg[current_index - 1].content = Vec::from(acc);
            }
            let parts: String = String::from(l)
                .chars()
                .filter(|x| x.is_numeric() || x.is_whitespace())
                .collect();
            let mut params: Vec<u32> = parts
                .split_whitespace()
                .map(|x| x.parse::<u32>().unwrap())
                .collect();
            let id = params[0];
            params.swap_remove(0);
            let bb = BasicBlock {
                id: id,
                mem_acc_nb: 0,
                succs: params,
                preds: Vec::new(),
                level: 0,
                content: Vec::new(),
            };
            cfg.push(bb);
            current_index += 1;
            acc = Vec::new();
        } else if l.starts_with(&['0', '9'][..]) {
            // this keeps only the lines that are
            // instructions (starting with the memory
            // content)
            acc.push(String::from(l));
        }
        acc
    });
    cfg
}

// fills the mem_acc_nb and the preds field
pub fn finish_filling_fields(cfg: &mut Vec<BasicBlock>) -> u32 {
    let mut vec_of_preds: Vec<Vec<u32>> = Vec::new();
    let mut max_level = 0;
    let mut levels: Vec<u32> = Vec::new();
    for _ in 0..cfg.len() {
        vec_of_preds.push(Vec::new());
        levels.push(0)
    }
    for bb in &mut *cfg {
        for i in 0..bb.succs.len() {
            vec_of_preds[bb.succs[i] as usize].push(bb.id);
            levels[bb.succs[i] as usize] =
                max(levels[bb.succs[i] as usize], levels[bb.id as usize] + 1);
        }
    }
    for mut bb in &mut *cfg {
        let nb = bb.content.iter().fold(0, |mut acc, l| {
            let parts: Vec<_> = l.split_whitespace().collect();
            if parts[1].starts_with("str") || parts[1].starts_with("ldr") {
                acc += 1; // counts the number of lines starting with str or ldr corresponding to
                          // memory accesses
            }
            acc
        });
        bb.mem_acc_nb = nb;
        bb.level = levels[bb.id as usize];
        if levels[bb.id as usize] > max_level {
            max_level = levels[bb.id as usize];
        }
        for i in 0..vec_of_preds[bb.id as usize].len() {
            bb.preds.push(vec_of_preds[bb.id as usize][i]);
        }
    }
    max_level
}
