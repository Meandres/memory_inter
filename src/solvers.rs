use crate::structs::*;
use good_lp::*;

// Computes the wcet of a basic block with the
pub fn get_exec_time_bb(content: &Vec<String>) -> u32 {
    content.iter().fold(0, |mut acc, l| {
        let instruction = l.split_whitespace().nth(1).unwrap();
        if instruction.starts_with("b") {
            acc += 2;
        } else if instruction.starts_with("str") || instruction.starts_with("ldr") {
            acc += 5;
        } else {
            acc += 1;
        }
        acc
    })
}

// Returns a constraint. During the parsing of the cfg, each basic block is given a level. The
// level of a basic block is equal to the maximum between the level of its parents (starting at 0
// for the first basic block. The constraint is then that the sum of all the blocks at a level is
// equal to the sum of all the basic blocks at the nex level.
pub fn level_constraint(level: u32, vars: &Vec<Variable>, cfg: &Vec<BasicBlock>) -> Constraint {
    let mut rhs = Expression::with_capacity(2);
    let mut lhs = Expression::with_capacity(2);
    for bb in cfg {
        if bb.level == level {
            lhs.add_mul(1, vars[bb.id as usize]);
        }
        if bb.level == level + 1 {
            rhs.add_mul(1, vars[bb.id as usize]);
        }
    }
    let res: Constraint = lhs.eq(rhs);
    res
}

// Mostly some code to use the good_lp crate.
pub fn worst_memory_access_path(cfg: &Vec<BasicBlock>, max_level: u32) -> Sol {
    variables! {problem: 0<= vars[cfg.len()] (integer) <= 1; }; // creates the variables
                                                                // corresponding to the number of
                                                                // execution of each basic block.
                                                                // Variables must be either 0 or 1
                                                                // since there is no loop in the
                                                                // cfg given.
    let objective: Expression = vars.iter().zip(cfg.iter()).fold(
        Expression::with_capacity(cfg.len()),
        |mut expr, (var, bb)| {
            expr.add_mul(bb.mem_acc_nb, var);
            expr
        },
    ); // defines the objective (ie. the sum of all the variables multiplied by the number of
       // memory accesses in the corresponding basic block
    let mut model = problem.maximise(objective.clone()).using(default_solver);
    for l in 0..max_level {
        model.add_constraint(level_constraint(l, &vars, &cfg));
    }
    let res: Constraint = Expression::from_other_affine(1).eq(vars[0]);
    model.add_constraint(res);
    let solution = model.solve().unwrap();
    let aff_expr = solution.eval(&objective);
    let mut path: Vec<u32> = Vec::new();
    for v in vars {
        path.push(solution.value(v) as u32);
    }
    Sol {
        solution: Box::new(solution),
        obj_val: aff_expr,
        path: path,
    }
}

// Same with wcet
pub fn worst_execution_time_path(cfg: &Vec<BasicBlock>, max_level: u32) -> Sol {
    variables! {problem: 0<= vars[cfg.len()] (integer) <= 1; };
    let objective: Expression = vars.iter().zip(cfg.iter()).fold(
        Expression::with_capacity(cfg.len()),
        |mut expr, (var, bb)| {
            expr.add_mul(get_exec_time_bb(&bb.content) as f64, var);
            expr
        },
    );
    let mut model = problem.maximise(objective.clone()).using(default_solver);
    for l in 0..max_level {
        model.add_constraint(level_constraint(l, &vars, &cfg));
    }
    let res: Constraint = Expression::from_other_affine(1).eq(vars[0]);
    model.add_constraint(res);
    let solution = model.solve().unwrap();
    let aff_expr = solution.eval(&objective);
    let mut path: Vec<u32> = Vec::new();
    for v in vars {
        path.push(solution.value(v) as u32);
    }
    Sol {
        solution: Box::new(solution),
        obj_val: aff_expr,
        path: path,
    }
}
