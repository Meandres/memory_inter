use good_lp::*;

// Structures to store the different form of data handled during the analysis.
// All of these structs are then bundled in Vec (growable arrays in Rust)
pub struct BasicBlock {
    pub id: u32,
    pub mem_acc_nb: u32,
    pub succs: Vec<u32>,
    pub preds: Vec<u32>,
    pub level: u32,
    pub content: Vec<String>,
}

pub struct Sol {
    pub solution: Box<dyn Solution>,
    pub obj_val: f64, // contains the value (the max)
    pub path: Vec<u32>,
}

pub struct Program {
    pub name: String,
    pub wcet: Sol,
    pub wcma: Sol,
}
