# Memory interference 

Student : Ilya Meignan--Masson

Please look at the comments in the code to understand better the choices and the design

## Level 0 :

The executable will output if the two paths are similar or different. On the two programs given, the paths are different.

If the tasks are executed in parallel, there should be some intereferences during the access to main memory.

## Level 1 :

This timing model is far from the real execution because it doesn't include the cache. Two programs running on two cores could be quite memory intensive but over a section of memory small enough to fit in a a few lines of cache which in turn would mean that once the data is in cache, there is no more accesses to the main memory and thus no extra delay due to the multi-core architecture. Another reason is that the function sums the wcet with the wcma even though the two paths are more frequently not identical. This produces an upper bound.

A TDMA bus analysis would require to include the fact that multiple cores can access the memory at the same time.

## Level 2 :

This finer analysis gives a better understanding of the execution of programs in parallel. For example, we can see that with any number of cores, executing only the TDF program greatly increases the execution time of the program compared to when we executed at least one DoorControl program.

One way of improving the analysis could be to synchronize at a finer grain for example at the basic block level or even at the instruction level. This would yield way more precise results. However, we would need to make a lot of assumptions on the behavior of the hardware running the programs (cores, buses, arbiter, etc.). It would be quite difficult to produce accurate results.
Another analysis related to multi-core problems could be the analysis of the share L3 cache.

